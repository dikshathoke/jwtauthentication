package controller

import (
	"html/template"
	"jwt2/models"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateProduct godoc
// @Summary CreateProduct endpoint is used by the supervisor role user to create a new product.
// @Description CreateProduct endpoint is used by the supervisor role user to create a new product
// @Router /api/v1/auth/product/create [post]
// @Tags product
// @Accept json
// @Produce json
// @Param name formData string true "name of the product"
// @Param category_id formData int true "id of the category"
func CreateBook(c *gin.Context) {

	var existingBook models.Book
	claims := jwt.ExtractClaims(c)
	user_email, _ := claims["email"]
	var User models.User
	var category models.Category

	// Check if the current user had admin role.
	if err := models.DB.Where("email = ? AND user_role_id=2", user_email).First(&User).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Book can only be added by supervisor user"})
		return
	}

	c.Request.ParseForm()

	if c.PostForm("name") == "" {
		ReturnParameterMissingError(c, "name")
		return
	}
	if c.PostForm("category_id") == "" {
		ReturnParameterMissingError(c, "category_id")
		return
	}

	book_title := template.HTMLEscapeString(c.PostForm("name"))
	category_id := template.HTMLEscapeString(c.PostForm("category_id"))

	// Check if the book already exists.
	err := models.DB.Where("title = ?", book_title).First(&existingBook).Error
	if err == nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "book already exists."})
		return
	}

	// Check if the category exists
	err = models.DB.First(&category, category_id).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "category does not exists."})
		return
	}
	cat := models.Book{
		Title:      book_title,
		CategoryId: category.ID,
		CreatedBy:  User.ID,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}

	err = models.DB.Create(&cat).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"id":   cat.ID,
		"name": cat.Title,
	})

}

// UpdateProduct godoc
// @Summary UpdateProduct endpoint is used by the supervisor role user to update a new product.
// @Description UpdateProduct endpoint is used by the supervisor role user to update a new product
// @Router /api/v1/auth/product/:id/ [PATCH]
// @Tags product
// @Accept json
// @Produce json
// @Param name formData string true "name of the product"
func UpdateBook(c *gin.Context) {
	var existingBook models.Book
	var updateBook models.Book
	claims := jwt.ExtractClaims(c)
	user_email, _ := claims["email"]
	var User models.User

	// Check if the current user had admin role.
	if err := models.DB.Where("email = ? AND user_role_id=2", user_email).First(&User).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "book can only be updated by supervisor user"})
		return
	}

	// Check if the book already exists.
	err := models.DB.Where("id = ?", c.Param("id")).First(&existingBook).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "book doesnot exists."})
		return
	}

	if err := c.ShouldBindJSON(&updateBook); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	models.DB.Model(&existingBook).Updates(updateBook)

}

type ReturnedBook struct {
	ID         int    `json:"id,string"`
	Title      string `json:"name"`
	CategoryId int    `json:"category_id"`
}

// GetProduct godoc
// @Summary GetProduct endpoint is used to get info of a product..
// @Description GetProduct endpoint is used to get info of a product.
// @Router /api/v1/auth/product/:id/ [get]
// @Tags product
// @Accept json
// @Produce json
// @Param name formData string true "name of the product"
func GetBook(c *gin.Context) {
	var existingBook models.Book

	// Check if the book already exists.
	err := models.DB.Where("id = ?", c.Param("id")).First(&existingBook).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "book doesnot exists."})
		return
	}

	// GET FROM CACHE FIRST
	c.JSON(http.StatusOK, gin.H{"book": existingBook})
	return
}

// ListAllProduct godoc
// @Summary ListAllProduct endpoint is used to list all products.
// @Description API Endpoint to register the user with the role of Supervisor or Admin.
// @Router /api/v1/auth/product/ [get]
// @Tags product
// @Accept json
// @Produce json
func ListAllBook(c *gin.Context) {

	claims := jwt.ExtractClaims(c)
	user_email, _ := claims["email"]
	var User models.User
	var Book []models.Book
	var ExistingBook []ReturnedBook

	if err := models.DB.Where("email = ?", user_email).First(&User).Error; err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "unauthorized"})
		return
	}
	models.DB.Model(Book).Find(&ExistingBook)
	c.JSON(http.StatusOK, ExistingBook)
	return
}

func generateFilePath(id string, extension string) string {
	// Generate random file name for the new uploaded file so it doesn't override the old file with same name
	newFileName := uuid.New().String() + extension

	projectFolder, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	localS3Folder := projectFolder + "/locals3/"
	bookImageFolder := localS3Folder + id + "/"

	if _, err := os.Stat(bookImageFolder); os.IsNotExist(err) {
		os.Mkdir(bookImageFolder, os.ModeDir)
	}

	imagePath := bookImageFolder + newFileName
	return imagePath
}

type UploadedFile struct {
	Status   bool
	BookID   int
	Filename string
	Path     string
	Err      string
}

func SaveToBucket(c *gin.Context, f *multipart.FileHeader, extension string, filename string) UploadedFile {
	/*
		whitelist doctionary for extensions
		golang doesnot support "for i in x" construct like python,
		Iterating the list would be expensive, thus we need to use a struct to prevent for loop.
	*/
	acceptedExtensions := map[string]bool{
		".png":  true,
		".jpg":  true,
		".JPEG": true,
		".PNG":  true,
	}
	id, _ := strconv.Atoi(c.Param("id"))

	if !acceptedExtensions[extension] {
		return UploadedFile{Status: false, BookID: id, Filename: filename, Err: "Invalid Extension"}
	}

	filePath := generateFilePath(c.Param("id"), extension)
	err := c.SaveUploadedFile(f, filePath)

	if err == nil {
		return UploadedFile{
			Status:   true,
			BookID:   id,
			Filename: filename,
			Path:     filePath,
			Err:      "",
		}
	}
	return UploadedFile{Status: false, BookID: id, Filename: filename, Err: ""}
}

// UploadProductImages godoc
// @Summary UploadProductImages endpoint is used to add images to product.
// @Description API Endpoint to register the user with the role of Supervisor or Admin.
// @Router /api/v1/auth/product/:id/image/upload [post]
// @Tags product
// @Accept json
// @Produce json
func UploadBookImages(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))

	if !IsSupervisor(c) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Book Image can only be added by supervisor"})
		return
	}

	if !DoesBookExist(id) {
		c.JSON(http.StatusNotFound, "book does not exist")
		return
	}

	form, _ := c.MultipartForm()
	files := form.File["file"]

	var SuccessfullyUploadedFiles []UploadedFile
	var UnSuccessfullyUploadedFiles []UploadedFile
	var BookImages []models.BookImage

	for _, f := range files {
		//save the file to specific dst
		extension := filepath.Ext(f.Filename)
		uploaded_file := SaveToBucket(c, f, extension, f.Filename)
		if uploaded_file.Status {
			SuccessfullyUploadedFiles = append(SuccessfullyUploadedFiles, uploaded_file)
			BookImages = append(BookImages, models.BookImage{
				URL:       uploaded_file.Path,
				BookId:    uploaded_file.BookID,
				CreatedAt: time.Now(),
			})

		} else {
			UnSuccessfullyUploadedFiles = append(UnSuccessfullyUploadedFiles, uploaded_file)
		}
	}
	models.DB.Create(&BookImages)

	c.JSON(http.StatusOK, gin.H{
		"successful": SuccessfullyUploadedFiles, "unsuccessful": UnSuccessfullyUploadedFiles,
	})

}
