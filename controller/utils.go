package controller

import (
	"errors"
	"fmt"
	"jwt2/models"
	"log"
	"unicode"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

// IsPasswordStrong Check password strength
func IsPasswordStrong(password string) (bool, error) {
	var IsLength, IsUpper, IsLower, IsNumber, IsSpecial bool

	if len(password) < 6 {
		return false, errors.New("password length should be more then 6")
	}
	IsLength = true

	for _, v := range password {
		switch {
		case unicode.IsNumber(v):
			IsNumber = true

		case unicode.IsUpper(v):
			IsUpper = true

		case unicode.IsLower(v):
			IsLower = true

		case unicode.IsPunct(v) || unicode.IsSymbol(v):
			IsSpecial = true

		}
	}

	if IsLength && IsLower && IsUpper && IsNumber && IsSpecial {
		return true, nil
	}

	return false, errors.New("password validation failed")

}

// HashPassword returns the hashed password, which can be stored in the database.
func HashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 8)
	if err != nil {
		log.Fatal("Error in Hashing")
		return "", err
	}
	return string(hashedPassword), err
}

// DoesUserExist is a helper function which checks if the user already exists in the user table or not.
func DoesUserExist(email string) bool {
	var users []models.User
	err := models.DB.Where("email=?", email).First(&users).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return false
		}
	}
	return true
}

// DoesBookExist is a helper function which checks if the user already exists in the user table or not.
func DoesBookExist(ID int) bool {
	var book []models.Book
	err := models.DB.Where("id=?", ID).First(&book).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return false
		}
	}
	return true
}

func CheckCredentials(username, password string, db *gorm.DB) bool {
	// db := c.MustGet("db").(*gorm.DB)
	// var db *gorm.DB
	var User models.User
	// Store user supplied password in mem map
	var expectedpassword string
	// check if the email exists

	err := db.Where("email = ? OR mobile = ?", username, username).First(&User).Error
	if err == nil {
		// User Exists...Now compare his password with our password
		expectedpassword = User.Password
		if err = bcrypt.CompareHashAndPassword([]byte(expectedpassword), []byte(password)); err != nil {
			// If the two passwords don't match, return a 401 status
			log.Println("User is Not Authorized")
			return false
		}
		// User is AUthenticates, Now set the JWT Token
		fmt.Println("User Verified")
		return true
	} else {
		// returns an empty array, so simply pass as not found, 403 unauth
		log.Fatal("ERR ", err)

	}
	return false
}

func IsAdmin(c *gin.Context) bool {
	claims := jwt.ExtractClaims(c)
	fmt.Println(claims)
	username := claims["email"]
	fmt.Println(username)
	var User models.User

	// Check if the current user had admin role.
	if err := models.DB.Where("email = ? AND user_role_id=1", username).First(&User).Error; err != nil {
		return false
	}
	return true
}

func IsSupervisor(c *gin.Context) bool {
	claims := jwt.ExtractClaims(c)
	username, _ := claims["email"]
	var User models.User

	// Check if the current user had admin role.
	if err := models.DB.Where("email = ? AND user_role_id=2", username).First(&User).Error; err != nil {
		return false
	}
	return true
}

// var Flag string

// func IsMobile(c *gin.Context) bool {
// 	var username string
// 	var count int64

// 	flag := strings.Index(username, "@")

// 	if flag == -1 {
// 		Flag = "mobile"
// 		fmt.Println("User Login With Mobile")
// 	} else {
// 		Flag = "email"
// 		fmt.Println("User Login with Email")
// 	}
// 	if Flag == "email" {
// 		models.DB.Where("email = ?", username).Find(&users).Count(&count)
// 		if count == 0 {
// 			return nil, jwt.ErrFailedAuthentication
// 		}
// 	} else if Flag == "mobile" {
// 		models.DB.Where("mobile = ?", username).Find(&users).Count(&count)
// 		if count == 0 {
// 			return nil, jwt.ErrFailedAuthentication
// 		}
// 	}
// }
