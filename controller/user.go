package controller

import (
	"fmt"
	"html/template"
	"jwt2/models"
	"net/http"
	"strings"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
)

var Flag string

// ALl controller logic related to the users .
// Available endpoints - Signup, Profile, etc

// TempUser is a temperory struct to validate the user input.

type TempUser struct {
	Email           string `json:"email" binding:"required"`
	FirstName       string `json:"first_name" binding:"required"`
	LastName        string `json:"last_name" binding:"required"`
	Password        string `json:"password" binding:"required"`
	ConfirmPassword string `json:"confirm_password" binding:"required"`
	Mobile          string `json:"mobile" binding:"required"`
}

// Returns a custom user input
func ReturnParameterMissingError(c *gin.Context, parameter string) {
	var err = fmt.Sprintf("Required parameter %s missing.", parameter)
	c.JSON(http.StatusBadRequest, gin.H{"error": err})
}

// @Summary Signup endpoint is used for customer registeration. ( Supervisors/admin can be added only by admin. )
// @Description API Endpoint to register the user with the role of customer.
// @Router /api/v1/signup [post]
// @Tags auth
// @Accept json
// @Produce json
// @Param email formData string true "Email of the user"
// @Param first_name formData string true "First name of the user"
// @Param last_name formData string true "Last name of the user"
// @Param password formData string true "Password of the user"
// @Param confirm_password formData string true "Confirm password."

func Signup(c *gin.Context) {
	var tempUser TempUser
	var Role models.UserRole

	c.Request.ParseForm()
	paramList := []string{"email", "first_name", "last_name", "password", "confirm_password", "mobile"}
	missing := false
	for _, param := range paramList {
		if c.PostForm(param) == "" {
			missing = true
			ReturnParameterMissingError(c, param)

		}
	}
	if missing {
		return
	}

	tempUser.Email = template.HTMLEscapeString(c.PostForm("email"))
	tempUser.FirstName = template.HTMLEscapeString(c.PostForm("first_name"))
	tempUser.LastName = template.HTMLEscapeString(c.PostForm("last_name"))
	tempUser.Password = template.HTMLEscapeString(c.PostForm("password"))
	tempUser.ConfirmPassword = template.HTMLEscapeString(c.PostForm("confirm_password"))
	tempUser.Mobile = template.HTMLEscapeString(c.PostForm("mobile"))

	// Check if both passwords are same.
	if tempUser.Password != tempUser.ConfirmPassword {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Both passwords do not match."})
	}

	// check if the password is strong and matches the password policy
	// length > 8, atleast 1 upper case, atleast 1 lower case, atleast 1 symbol
	ispasswordstrong, _ := IsPasswordStrong(tempUser.Password)
	if !ispasswordstrong {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Password is not strong."})
		return
	}

	// Check if the user already exists.
	if DoesUserExist(tempUser.Email) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User already exists."})
		return
	}

	encryptedPassword, error := HashPassword(tempUser.Password)
	if error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Some error occoured."})
		return
	}

	err := models.DB.Where("role= ?", "customer").First(&Role).Error
	if err != nil {
		fmt.Println("err ", err.Error())
		return
	}

	SanitizedUser := models.User{
		FirstName:  tempUser.FirstName,
		LastName:   tempUser.LastName,
		Email:      tempUser.Email,
		Password:   encryptedPassword,
		Mobile:     tempUser.Mobile,
		UserRoleID: Role.Id,
		CreatedAt:  time.Now(),
		IsActive:   true,
	}

	errs := models.DB.Create(&SanitizedUser).Error
	if errs != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Some error occoured."})
		return
	}
	c.JSON(http.StatusCreated, gin.H{"msg": "User created successfully"})
}

// type login struct {
// 	Username string `form:"username" json:"username" binding:"required"`
// 	Password string `form:"password" json:"password" binding:"required"`
// }

// Signin godoc
// @Summary Signin endpoint is used by the user to login.
// @Description API Endpoint to register the user with the role of customer.
// @Router /api/v1/signin [post]
// @Tags auth
// @Accept json
// @Produce json
// @Param login formData login true "Credentials of the user"

func Signin(c *gin.Context) (interface{}, error) {
	/* THIS FUNCTION IS USED BY THE AUTH MIDDLEWARE. */
	// var loginVals login
	// var User User

	var users []models.User
	var count int64
	// var user models.User

	// if err := c.ShouldBind(&loginVals); err != nil {
	// 	return "", jwt.ErrMissingLoginValues
	// }

	username := template.HTMLEscapeString(c.PostForm("username"))
	password := template.HTMLEscapeString(c.PostForm("password"))

	// for _, i := range username {
	// 	i
	// }
	//   convert this into helper function

	flag := strings.Index(username, "@")

	if flag == -1 {
		Flag = "mobile"
		fmt.Println("User Login With Mobile")
	} else {
		Flag = "email"
		fmt.Println("User Login with Email")
	}

	//email := loginVals.Email
	if Flag == "email" {
		models.DB.Where("email = ?", username).Find(&users).Count(&count)
		if count == 0 {
			return nil, jwt.ErrFailedAuthentication
		}
	} else if Flag == "mobile" {
		models.DB.Where("mobile = ?", username).Find(&users).Count(&count)
		if count == 0 {
			return nil, jwt.ErrFailedAuthentication
		}
	}
	// First check if the user exist or not...
	// models.DB.Where("email = ?", email).Find(&users).Count(&count)
	// if count == 0 {
	// 	return nil, jwt.ErrFailedAuthentication
	// }
	if CheckCredentials(username, password, models.DB) {
		if Flag == "email" {
			return &models.User{
				Email: username,
			}, nil
		} else if Flag == "mobile" {
			return &models.User{
				Mobile: username,
			}, nil
		}
	}
	return nil, jwt.ErrFailedAuthentication
}

// func Signinm(c *gin.Context) (interface{}, error) {
// 	var loginVals1 loginm

// 	var users []models.User
// 	var count int64
// 	if err := c.ShouldBind(&loginVals1); err != nil {
// 		return "", jwt.ErrMissingLoginValues
// 	}
// 	mobile := loginVals1.Mobile
// 	models.DB.Where("mobile = ?", mobile).Find(&users).Count(&count)
// 	if count == 0 {
// 		return nil, jwt.ErrFailedAuthentication
// 	}
// 	if CheckCredentials(loginVals1.Mobile, loginVals1.Password, models.DB) == true {
// 		return &models.User{
// 			Mobile: mobile,
// 		}, nil
// 	}
// 	return nil, jwt.ErrFailedAuthentication
// }

// CreateSupervisorOrAdmin godoc
// @Summary CreateSupervisor endpoint is used by the admin role user to create a new admin or supervisor account.
// @Description API Endpoint to register the user with the role of Supervisor or Admin.
// @Router /api/v1/auth/supervisor/create [post]
// @Tags supervisor
// @Accept json
// @Produce json
// @Param login formData TempUser true "Info of the user"

func CreateSupervisor(c *gin.Context) {
	if !IsAdmin(c) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "unauthorized"})
		return
	}

	// Create a user with the role of supervisor.
	var tempUser TempUser
	var Role models.UserRole

	c.Request.ParseForm()
	paramList := []string{"email", "first_name", "last_name", "password", "confirm_password"}

	for _, param := range paramList {
		if c.PostForm(param) == "" {
			ReturnParameterMissingError(c, param)
		}
	}

	tempUser.Email = template.HTMLEscapeString(c.PostForm("email"))
	tempUser.FirstName = template.HTMLEscapeString(c.PostForm("first_name"))
	tempUser.LastName = template.HTMLEscapeString(c.PostForm("last_name"))
	tempUser.Password = template.HTMLEscapeString(c.PostForm("password"))
	tempUser.ConfirmPassword = template.HTMLEscapeString(c.PostForm("confirm_password"))

	// Check if both passwords are same.
	if tempUser.Password != tempUser.ConfirmPassword {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Both passwords do not match."})
	}

	// check if the password is strong and matches the password policy
	// length > 8, atleast 1 upper case, atleast 1 lower case, atleast 1 symbol
	ispasswordstrong, _ := IsPasswordStrong(tempUser.Password)
	if ispasswordstrong {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Password is not strong."})
		return
	}

	// Check if the user already exists.
	if DoesUserExist(tempUser.Email) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User already exists."})
		return
	}

	encryptedPassword, error := HashPassword(tempUser.Password)
	if error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Some error occoured."})
		return
	}

	err := models.DB.Where("role= ?", "supervisor").First(&Role).Error
	if err != nil {
		fmt.Println("err ", err.Error())
		return
	}

	SanitizedUser := models.User{
		FirstName:  tempUser.FirstName,
		LastName:   tempUser.LastName,
		Email:      tempUser.Email,
		Password:   encryptedPassword,
		UserRoleID: Role.Id, //This endpoint will be used only for customer registeration.
		CreatedAt:  time.Now(),
		IsActive:   true,
	}

	errs := models.DB.Create(&SanitizedUser).Error
	if errs != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Some error occoured."})
		return
	}
	c.JSON(http.StatusCreated, gin.H{"msg": "User created successfully"})

	return
}

// CreateAdmin godoc
// @Summary CreateAdmin endpoint is used by the admin role user to create a new admin or vendor account.
// @Description API Endpoint to register the user with the role of Supervisor or Admin.
// @Router /api/v1/auth/admin/create [post]
// @Tags admin
// @Accept json
// @Produce json
// @Param login formData TempUser true "Info of the user"
func CreateAdmin(c *gin.Context) {

	if !IsAdmin(c) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "unauthorized"})
		return
	}
	// Create a user with the role of supervisor.
	var tempUser TempUser
	var Role models.UserRole

	c.Request.ParseForm()
	paramList := []string{"email", "first_name", "last_name", "password", "confirm_password"}

	for _, param := range paramList {
		if c.PostForm(param) == "" {
			ReturnParameterMissingError(c, param)
		}
	}

	tempUser.Email = template.HTMLEscapeString(c.PostForm("email"))
	tempUser.FirstName = template.HTMLEscapeString(c.PostForm("first_name"))
	tempUser.LastName = template.HTMLEscapeString(c.PostForm("last_name"))
	tempUser.Password = template.HTMLEscapeString(c.PostForm("password"))
	tempUser.ConfirmPassword = template.HTMLEscapeString(c.PostForm("confirm_password"))

	// Check if both passwords are same.
	if tempUser.Password != tempUser.ConfirmPassword {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Both passwords do not match."})
	}

	// check if the password is strong and matches the password policy
	// length > 8, atleast 1 upper case, atleast 1 lower case, atleast 1 symbol
	ispasswordstrong, _ := IsPasswordStrong(tempUser.Password)
	if !ispasswordstrong {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Password is not strong."})
		return
	}

	// Check if the user already exists.
	if DoesUserExist(tempUser.Email) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User already exists."})
		return
	}

	encryptedPassword, error := HashPassword(tempUser.Password)
	if error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Some error occoured."})
		return
	}

	err := models.DB.Where("role= ?", "admin").First(&Role).Error
	if err != nil {
		fmt.Println("err ", err.Error())
		return
	}

	SanitizedUser := models.User{
		FirstName:  tempUser.FirstName,
		LastName:   tempUser.LastName,
		Email:      tempUser.Email,
		Password:   encryptedPassword,
		UserRoleID: Role.Id, //This endpoint will be used only for customer registeration.
		CreatedAt:  time.Now(),
		IsActive:   true,
	}

	errs := models.DB.Create(&SanitizedUser).Error
	if errs != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Some error occoured."})
		return
	}
	c.JSON(http.StatusCreated, gin.H{"msg": "User created successfully"})

	return
}
