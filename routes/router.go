package routes

import (
	"jwt2/controller"
	_ "jwt2/docs"
	"jwt2/middleware"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

func PublicEndpoints(r *gin.RouterGroup, authMiddleware *jwt.GinJWTMiddleware) {
	// Generate public endpoints - [signup] - api/v1/signup

	r.POST("/signup", controller.Signup)
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.POST("/signin", authMiddleware.LoginHandler)

}

func AuthenticatedEndpoints(r *gin.RouterGroup, authMiddleware *jwt.GinJWTMiddleware) {
	// Generate Authenticated endpoints - [] - api/v1/auth/
	r.Use(authMiddleware.MiddlewareFunc())

	// User related endpoints
	r.POST("supervisor/create", controller.CreateSupervisor)
	r.POST("admin/create", controller.CreateAdmin)

	// Category related endpoints
	r.GET("category/", controller.ListAllCategories)
	r.POST("category/create", controller.CreateCategory)
	r.GET("category/:id", controller.GetCategory)
	r.PATCH("category/:id", controller.UpdateCatagory)

	// book related endpoints.
	r.GET("book/", controller.ListAllBook)
	r.POST("book/create", controller.CreateBook)
	r.POST("book/:id/image/upload", controller.UploadBookImages)
	r.GET("book/:id", controller.GetBook)
	r.PATCH("book/:id", controller.UpdateBook)
}

func GetRouter(router chan *gin.Engine) {
	gin.ForceConsoleColor()
	r := gin.Default()

	r.Use(cors.Default())
	authMiddleware, _ := middleware.GetAuthMiddleware()

	// Create a BASE_URL - /api/v1
	v1 := r.Group("/api/v1/")
	PublicEndpoints(v1, authMiddleware)
	// Group creates a new router group. You should add all the routes that have common middlewares or the same path prefix.
	// For example, all the routes that use a common middleware for authorization could be grouped.
	AuthenticatedEndpoints(v1.Group("auth"), authMiddleware)
	router <- r
}
