package middleware

import (
	"fmt"
	"jwt2/controller"
	"jwt2/models"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
)

// the jwt middleware

func GetAuthMiddleware() (*jwt.GinJWTMiddleware, error) {

	var Value string
	if controller.Flag == "email" {

		Value = "email"

	} else {
		Value = "mobile"
	}
	
	// var identityKey = "email"
	// var identityKey2 = "mobile"
	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{

		// Realm name to display to the user. Required.

		Realm:       "test zone",
		Key:         []byte("secret key"),
		Timeout:     time.Hour,
		MaxRefresh:  time.Hour,
		IdentityKey: Value,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*models.User); ok {
				if controller.Flag == "email" {
					return jwt.MapClaims{

						Value: v.Email,
					}

				} else {
					return jwt.MapClaims{

						Value: v.Mobile,
					}

				}

			}
			return jwt.MapClaims{}
		},

		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			fmt.Println(claims)
			return &models.User{
				Email: claims[Value].(string),
			}
		},

		Authenticator: controller.Signin,

		// Allow Users to access some special pages
		Authorizator: func(data interface{}, c *gin.Context) bool {
			if _, ok := data.(*models.User); ok {
				return true
			}
			return false
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		// TokenLookup is a string in the form of "<source>:<name>" that is used
		// to extract token from the request.
		// Optional. Default value "header:Authorization".
		// Possible values:
		// - "header:<name>"
		// - "query:<name>"
		// - "cookie:<name>"
		// - "param:<name>"

		TokenLookup: "header: Authorization, query: token, cookie: jwt",
		// TokenLookup: "query:token",
		// TokenLookup: "cookie:token",

		// TokenHeadName is a string in the header. Default value is "Bearer"
		TokenHeadName: "Bearer",

		// if sendcookie is true then token is automatic save in header
		SendCookie: true,

		// TimeFunc provides the current time. You can override it to use another time value. This is useful for testing or if your server uses a different time zone than your tokens.
		TimeFunc: time.Now,
	})

	if err != nil {

		return nil, err
	}
	return authMiddleware, nil
}
